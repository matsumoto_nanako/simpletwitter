package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

    @Override //login.jspを呼ぶ
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override //login.jspに表示
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//ログインしたユーザーのアカウント名かEmail・パスワードのパラメータを取得→本人かどうか判断・ユーザーidをしゅ
        String accountOrEmail = request.getParameter("accountOrEmail");
        String password = request.getParameter("password");

      //取得したEmailとパスワードを引数に渡して情報を取得

        User user = new UserService().select(accountOrEmail, password); //userに返ってきた戻り値を取得
        if (user == null) { //nullでかえってこればログイン不可（アカウント名かEmail・パスワードがなければログインに失敗）
            List<String> errorMessages = new ArrayList<String>(); //errorMssagesリストを生成
            errorMessages.add("ログインに失敗しました");
            request.setAttribute("errorMessages", errorMessages);
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }

        //リスト化したログイン情報を取得できた場合（userの中身がnullでなければ）
        HttpSession session = request.getSession(); //セッションを取得
        session.setAttribute("loginUser", user); //"loginUser"という名前でuser(リスト化したログイン情報）セッションに値をセット
        response.sendRedirect("./"); //TopServletに遷移
    }
}