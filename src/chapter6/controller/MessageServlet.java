package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.User;
import chapter6.service.MessageService;


//top.jspのsubmitから/messageのURLに対応するMessageServletに遷移
@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	//入力されたつぶやきのテキストを取得してDBのmessagesテーブルにデータを格納
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//エラーメッセージはトップ画面に表示させるためセッションに格納
        HttpSession session = request.getSession(); //セッションを取得
        List<String> errorMessages = new ArrayList<String>(); //erroreMessagesリストを生成

        String text = request.getParameter("text"); //top.jspのtextのパラメータを取得しStiring型のtextに格納
        if (!isValid(text, errorMessages)) { //falseが返ってきたとき
            session.setAttribute("errorMessages", errorMessages); //エラーメッセージをセッションにセット
            response.sendRedirect("./"); //リダイレクト
            return;
        }

        //バリデーションをしてtrueで返ってきた場合にテキスト情報をデータベースに格納
        Message message = new Message();
        message.setText(text); //うちこまれたテキスト情報をmessageにセット

        User user = (User) session.getAttribute("loginUser"); //ログインしているユーザーの情報を取得
        message.setUserId(user.getId()); //ログインしているユーザーのidをmessageにセット

        new MessageService().insert(message); //mmessageを引数にMessageServiceのinsertメソッドに渡す
        response.sendRedirect("./"); //リダイレクトでトップページへ遷移
    }

    //バリデーション（エラーがなければtrueを返す）
    private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isEmpty(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }
        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}