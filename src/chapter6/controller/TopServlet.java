package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserMessage;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {


	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {


		//テキストエリアの表示条件
		boolean isShowMessageForm = false;
		//LoginServletでセッションにセットした属性名"loginUser"でインスタンスuser（ログインユーザーの情報）を取得してuserに格納
		User user = (User) request.getSession().getAttribute("loginUser"); //user=ログインユーザーの情報リスト
		//ログインユーザーのオブジェクトが取得できた場合(nullではなかった場合)にはメッセージフォームを表示する
		if (user != null) {
			isShowMessageForm = true;
		}

		//パラメータとしてuser_idを受け取った場合は該当のuser_idのつぶやきのみ表示させる

		String userId = request.getParameter("user_id"); //top.jspからuser_idの値を取得してuserIdに格納
		//MessageServiceのselectメソッドにtop.jspから取得したユーザーIDを引数に渡してmessagesリストにいれる
		//ログインしているユーザーがつぶやいたメッセージとしてリスト化

		List<UserMessage> messages = new MessageService().select(userId);


		request.setAttribute("messages", messages);//該当するuserIdのつぶやきリストをmessagesとしてセット
		request.setAttribute("isShowMessageForm", isShowMessageForm); //isShowMessageForm=true or falseをセット);

		request.getRequestDispatcher("/top.jsp").forward(request, response);//top.jspに処理を渡す
	}

}