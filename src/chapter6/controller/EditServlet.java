package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.MessageService;


@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	//ログインユーザーが選択したつぶやきを表示する
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {


		String id = request.getParameter("id");
		Message messages = new MessageService().messageSelect(id);


		request.setAttribute("messages", messages);
		request.getRequestDispatcher("/edit.jsp").forward(request, response);
	}

	//打ち込まれたテキストを更新する
	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		Message message = getMessage(request);//更新したい新たなテキストを取得
		String text = request.getParameter("text");
		if (isValid(text, errorMessages)) {
            try {
                new MessageService().update(message); //userを引数にUserServiceのupdateメソッドに渡す＝アップデート
            } catch (NoRowsUpdatedRuntimeException e) {
                errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
            }
        }

		if (errorMessages.size() != 0) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}

		session.setAttribute("message", message);
		response.sendRedirect("./");
	}

	private Message getMessage(HttpServletRequest request) throws IOException, ServletException {

		Message message = new Message();

		message.setId(Integer.parseInt(request.getParameter("id")));
		message.setText(request.getParameter("text"));

		return message;
	}

	//バリデーション

	private boolean isValid(String text, List<String> errorMessages) {

		 if (StringUtils.isEmpty(text)) {
	            errorMessages.add("メッセージを入力してください");
	        } else if (140 < text.length()) {
	            errorMessages.add("140文字以下で入力してください");
	        }
	        if (errorMessages.size() != 0) {
	            return false;
	        }
	        return true;
	}


}
