package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

	//設定画面にもともとのログインユーザーの情報をDBから取得し表示する
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession(); //セッションを取得
        User loginUser = (User) session.getAttribute("loginUser"); //セッションから属性であるログインユーザーを取り出す
        User user = new UserService().select(loginUser.getId()); //ログインユーザーから取得したIDを引数にUserServiceに渡してuserに格納

        request.setAttribute("user", user); //リクエストにuserをキーにログインユーザー情報をセットする
        request.getRequestDispatcher("setting.jsp").forward(request, response);//setting.jspに処理を移す
    }

    //新たに打ち込まれたログインユーザーの情報を取得しDBの情報をアップデートしトップ画面に
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession(); //セッションの取得
        List<String> errorMessages = new ArrayList<String>(); //errorMessagesリストの作成

        User user = getUser(request); //userにuserの情報を格納

        //バリデーションをしてエラーがなければ（trueが返ってくれば）アップデートをする
        if (isValid(user, errorMessages)) {
            try {
                new UserService().update(user); //userを引数にUserServiceのupdateメソッドに渡す＝アップデート
            } catch (NoRowsUpdatedRuntimeException e) {
                errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
            }
        }

        if (errorMessages.size() != 0) { //errorMessagesリストに要素があるとき
            request.setAttribute("errorMessages", errorMessages); //エラーメッセージをセットする
            request.setAttribute("user", user); //userデータをセットする
            request.getRequestDispatcher("setting.jsp").forward(request, response); //setting.jspに処理を移す
            return;	 //処理を終了
        }

        session.setAttribute("loginUser", user); //セッションにloginUserをキーにuserデータをセット
        response.sendRedirect("./"); //リダイレクト
    }

    private User getUser(HttpServletRequest request) throws IOException, ServletException {

    	//ログインのあったユーザーの情報を取得して返す
        User user = new User();
        user.setId(Integer.parseInt(request.getParameter("id")));
        user.setName(request.getParameter("name"));
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setEmail(request.getParameter("email"));
        user.setDescription(request.getParameter("description"));
        return user;
    }

    //バリデーション（エラーがあればfalseを返す・エラーがなければtrueを返す）
    private boolean isValid(User user, List<String> errorMessages) {

        String name = user.getName();
        String account = user.getAccount();
        String email = user.getEmail();
        int id = user.getId();

        User selectUser = new UserService().select(account);

        if (!StringUtils.isEmpty(name) && (20 < name.length())) {
            errorMessages.add("名前は20文字以下で入力してください");
        }
        if (StringUtils.isEmpty(account)) {
            errorMessages.add("アカウント名を入力してください");
        } else if (20 < account.length()) {
        	errorMessages.add("アカウント名は20文字以下で入力してください");
        }
        //idが違う、かつアカウント名が同じ人を重複と認識
        if (selectUser != null && id != selectUser.getId()) {
        	errorMessages.add("アカウント名が重複しています");
        }
        if (!StringUtils.isEmpty(email) && (50 < email.length())) {
            errorMessages.add("メールアドレスは50文字以下で入力してください");
        }
        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }

}
